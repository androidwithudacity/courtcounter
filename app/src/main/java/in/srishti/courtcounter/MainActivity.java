package in.srishti.courtcounter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

// importing the enum defined in another file
import in.srishti.courtcounter.Team;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /**
     * Method to add three points to the current score of Team A
     * @param view
     */
    public void addThreePointsToTeamA (View view) {
        int current_scrore = this._getCurrentScore(Team.A);
        if (current_scrore >= 0) {
            current_scrore += 3;
            this._setCurrentScore(Team.A, current_scrore);
        }
    }

    /**
     * Method to add two points to the current score of Team A
     * @param view
     */
    public void addTwoPointsToTeamA (View view) {
        int current_scrore = this._getCurrentScore(Team.A);
        if (current_scrore >= 0) {
            current_scrore += 2;
            this._setCurrentScore(Team.A, current_scrore);
        }
    }

    /**
     * Method to add one point to the current score of Team A
     * @param view
     */
    public void addOnePointToTeamA (View view) {
        int current_scrore = this._getCurrentScore(Team.A);
        if (current_scrore >= 0) {
            current_scrore += 1;
            this._setCurrentScore(Team.A, current_scrore);
        }
    }

    /**
     * Method to add three points to the current score of Team B
     * @param view
     */
    public void addThreePointsToTeamB (View view) {
        int current_scrore = this._getCurrentScore(Team.B);
        if (current_scrore >= 0) {
            current_scrore += 3;
            this._setCurrentScore(Team.B, current_scrore);
        }
    }

    /**
     * Function to add two points to the current score of Team B
     * @param view
     */
    public void addTwoPointsToTeamB (View view) {
        int current_scrore = this._getCurrentScore(Team.B);
        if (current_scrore >= 0) {
            current_scrore += 2;
            this._setCurrentScore(Team.B, current_scrore);
        }
    }

    /**
     * Method to add one point to the current score of Team B
     * @param view
     */
    public void addOnePointToTeamB (View view) {
        int current_scrore = this._getCurrentScore(Team.B);
        if (current_scrore >= 0) {
            current_scrore += 1;
            this._setCurrentScore(Team.B, current_scrore);
        }
    }

    /**
     * Function to reset the scores of both teams
     * @param view
     */
    public void reset (View view) {
        this._setCurrentScore(Team.A, 0);
        this._setCurrentScore(Team.B, 0);
    }

    /**
     * Method to get the current score of the given team
     * @param teamName: name of the team
     * @return currentScore of the given team
     */
    private int _getCurrentScore(Team teamName) {
        int currentScore = -1;
        TextView scoreTextView = null;
        if (teamName != null) {
            switch (teamName) {
                case A:
                    scoreTextView = (TextView) findViewById(R.id.scoreTeamA_textview);
                    break;
                case B:
                    scoreTextView = (TextView) findViewById(R.id.scoreTeamB_textview);
                    break;
            }
            if (scoreTextView != null) {
                currentScore = Integer.parseInt(scoreTextView.getText().toString());
            }
        }
        return currentScore;
    }

    /**
     * Method to set (update) the current score of the given team
     * @param teamName: name of the team
     * @param score: value to which the score is updated to
     */
    private void _setCurrentScore(Team teamName, int score) {
        if (teamName != null) {
            TextView scoreTextView = null;
            switch (teamName) {
                case A:
                    scoreTextView = (TextView) findViewById(R.id.scoreTeamA_textview);
                    break;
                case B:
                    scoreTextView = (TextView) findViewById(R.id.scoreTeamB_textview);
                    break;
            }
            if (scoreTextView != null) {
                scoreTextView.setText(Integer.toString(score));
            }
        }
    }
}